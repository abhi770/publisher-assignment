import Content from "./components/Content";
import Header from "./components/Header";
import ImageSection from "./components/ImageSection";
import Profile from "./components/Profile";
import Tags from "./components/Tags";
import LC from "./components/LC";
import Comments from "./components/Comments";
import CommReply from "./components/CommReply";

function App() {
  return (
    <div className="App">
      <Header/>
      <Profile/>
      <Content/>
      <Tags/>
      <ImageSection/>
      <LC/>
      <Comments/>
      <CommReply/>
    </div>
  );
}

export default App;
