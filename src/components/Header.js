import React from 'react'
import Arrow from '../Images/image1.svg'
import './Header.css'
import Notification from '../Images/Bell.svg';

const Header = () => {
  return (
    <div className='rectangle'>
      <img className='arrow' src={Arrow} alt='Back Button'/> 
      <p className='header_content'>자유톡</p>
      <img src={Notification} className='noti' alt='notification' />
    </div>
  )
}

export default Header
