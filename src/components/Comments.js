import React from "react";
import "./Comments.css";
import profile from "../Images/profile.svg";
import Tick from "../Images/Tick.png";
import Dots from "../Images/Dots.svg";
import Heart from '../Images/Heart.svg'
import Commentss from '../Images/Comment.svg'

const Comments = () => {
  return (
    <div className="wrapper">
      <div className="comments_sec">
        <div className="comm_det">
          <img src={profile} alt="profile_pic" className="profile_pic" />
          <div className="bold name">안녕 나 응애</div>
          <img className="cover chu chi" src={Tick} alt="tick" />{" "}
          <div className="color_cng name"> 1일전</div>
        </div>
        <div>
          <img alt='tags' src={Dots} />
        </div>
      </div>
      <div className="comment">
        어머 제가 있던 테이블이 제일 반응이 좋았나보네요🤭 우짤래미님도
        아시겠지만 저도 일반인 몸매 그 이상도 이하도 아니잖아요?! 그런 제가
        기꺼이 도전해봤는데 생각보다 괜찮았어요! 오늘 중으로 라이브 리뷰
        올라온다고 하니 꼭 봐주세용~!
        <div className="comm_lc">
            <img src={Heart}  alt="likes"/> 
            <p>5</p>
            <img src={Commentss}  alt="comments"/>
            <p>5</p>
        </div>
      </div>
    </div>
  );
};

export default Comments;
