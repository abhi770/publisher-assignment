import React from "react";
import "./Profile.css";
import ProfileImg from "../Images/profile.svg";
import Tick from "../Images/Tick.png";

const Profile = () => {
  return (
    <div className="profile">
      <div className="left_con">
        <img className="profile_img cover" src={ProfileImg} alt="profile_img" />
        <div className="details">
          <div className="upper_detail">
            <div className="bold">안녕 나 응애</div>
            <img  style={{width:20}} className="cover chu" src={Tick} alt="tick" /> <div style={{paddingTop:3,paddingLeft:4}} className="color_cng"> 1일전</div>
          </div>
          <div className="weight">163cm . 53kg</div>
        </div>
      </div>
      <div className="right_con">
        <button className="right_con_btn">팔로우</button>
      </div>
    </div>
  );
};

export default Profile;
