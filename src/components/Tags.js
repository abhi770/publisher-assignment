import React from 'react'
import './Tags.css'

const Tags = () => {
  return (
    <div className='tags_section'>
      <button className='tag'>#2023</button>
      <button className='tag'>#TODAYISMONDAY</button>
      <button className='tag'>#TOP</button>
      <button className='tag'>#POPS!</button>
      <button className='tag'>#WOW</button>
      <button className='tag'>#ROW</button>
    </div>
  )
}

export default Tags
