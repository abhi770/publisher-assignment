import React from 'react'
import Heart from '../Images/Heart.svg'
import Comment from '../Images/Comment.svg'
import Dots from '../Images/Dots.svg'
import Bkmrk from '../Images/Bkmrk.svg'
import './LC.css';

const LC = () => {
  return (
    <>
    <div className='LC'>
      <img src={Heart} alt='Likes'></img>
      <p>5</p>
      <img src={Comment} alt='Likes'></img>
      <p>5</p>
      <img src={Bkmrk} alt='Likes'></img>
      <img src={Dots} alt='Likes'></img>
    </div>
    <div style={{marginTop:4,marginBottom:8}} className='divider'/>
    </>
  )
}

export default LC
