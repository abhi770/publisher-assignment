import React from 'react'
import './CommReply.css'
import Profile2 from '../Images/Profile2.svg';
import Dots from '../Images/Dots.svg';
import Heart from '../Images/Heart.svg'
import Gallery from '../Images/Gallery.svg'

const CommReply = () => {
  return (
    <>
    
      <div className='wrapper1'>
        <div className='upper_sect'>
          <div className='detailss'>
              <img alt='Profile ' className='profile_image' src={Profile2}  />
              <p className='profile2_name'>ㅇㅅㅇ</p>
              <p className='profile2_name_cntd'>1일전</p>
          </div>
          <div className='comments_dots'>
              <img  src={Dots} alt='dots' className='dots'/>
          </div>
        </div>
        <div className='comment2_content'>
        오 대박! 라이브 리뷰 오늘 올라온대요? 챙겨봐야겠다
        </div>
        <div className='likes_on_reply'>
          <img src={Heart} alt='likes' className='likes_pic'/> <p>5</p>
        </div>
      </div>
      <div className='divider'/>
      <div className='type_comm'>
        <div className='type_comm2'>
        <img src={Gallery} alt='gallery'/>
        <p className='placeholder'>댓글을 남겨주세요.</p>
        </div>
        <p>등록</p>
      </div>
    </>
  )
}

export default CommReply
